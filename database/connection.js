const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();

const { DATABASE_HOST, DATABASE_NAME } = process.env;

const URL = `mongodb://${DATABASE_HOST}/${DATABASE_NAME}`;

module.exports = () => {
  mongoose
  .connect(URL)
  .then(() => console.log("Database Connected!"))
  .catch((error) => console.error("Database Connection Failed: ", error));
};