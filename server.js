const express = require("express");
const dotenv = require("dotenv");
const database = require("./database/connection");

const app = express();

app.use(express.json());

dotenv.config();

const PORT = process.env.SERVER_PORT || 9000;

app.use("/", require("./routes/products"));

app.listen(PORT, (error) => {
  if (error) console.error(error);

  console.log(`Server Running on Port ${PORT}`);

  database();
});