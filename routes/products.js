const router = require("express").Router();
const productController = require("../controllers/products");

router
  .route("/")
  .get(productController.browse)
  .post(productController.add);

router
  .route("/:id")
  .get(productController.read)
  .put(productController.edit)
  .delete(productController.destroy);

module.exports = router;