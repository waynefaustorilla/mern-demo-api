const Products = require("./../models/Products");

module.exports.browse = async (request, response, next) => {
  const products = await Products.find({});

  return response.json({ products: products });
};

module.exports.read = async (request, response, next) => {
  try {
    const product = await Products.findById(request.params.id);

    return response.send({ product });
  } catch (error) {
    return response.status(404).send({ message: "Product Not Found" });
  }
};

module.exports.edit = async (request, response, next) => {
  try {
    const product = await Products.findByIdAndUpdate(request.params.id, {
      name: request.body.name,
      description: request.body.description,
      quantity: request.body.quantity,
      price: request.body.price
    }, { new: true });

    return response.json({ product: product });
  } catch (error) {
    return response.send({ error });
  }
};

module.exports.add = async (request, response, next) => {
  const { name, description, quantity, price } = await request.body;

  const product = await Products.create({ name, description, quantity, price });

  return response.status(201).json({ product });
};

module.exports.destroy = async (request, response, next) => {
  try {
    const product = await Products.findByIdAndDelete(request.params.id);

    return response.json({ product: product });
  } catch (error) {
    return response.status(404).send({ message: "Product Not Found" });
  }
};