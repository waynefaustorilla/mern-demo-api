const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product Name is Required"]
  },
  description: {
    type: String,
    required: false
  },
  quantity: {
    type: Number,
    required: [true, "Product Quantity is Required"]
  },
  price: {
    type: Number,
    required: [true, "Product Name is Required"]
  }
});

module.exports = mongoose.model("Product", schema);